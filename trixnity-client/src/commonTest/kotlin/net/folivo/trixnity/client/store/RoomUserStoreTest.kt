package net.folivo.trixnity.client.store

import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import net.folivo.trixnity.client.NoopRepositoryTransactionManager
import net.folivo.trixnity.client.store.repository.RoomUserRepository
import net.folivo.trixnity.core.model.EventId
import net.folivo.trixnity.core.model.RoomId
import net.folivo.trixnity.core.model.UserId
import net.folivo.trixnity.core.model.events.Event.StateEvent
import net.folivo.trixnity.core.model.events.m.room.MemberEventContent
import net.folivo.trixnity.core.model.events.m.room.Membership.JOIN
import net.folivo.trixnity.core.model.events.m.room.Membership.LEAVE

class RoomUserStoreTest : ShouldSpec({
    lateinit var roomUserRepository: RoomUserRepository
    lateinit var storeScope: CoroutineScope
    lateinit var cut: RoomUserStore

    beforeTest {
        storeScope = CoroutineScope(Dispatchers.Default)
        roomUserRepository = InMemoryTwoDimensionsStoreRepository()
        cut = RoomUserStore(roomUserRepository, NoopRepositoryTransactionManager, storeScope)
    }
    afterTest {
        storeScope.cancel()
    }

    val roomId = RoomId("room", "server")
    val aliceId = UserId("alice", "server")
    val bobId = UserId("bob", "server")
    val aliceUser = RoomUser(
        roomId = roomId,
        userId = aliceId,
        name = "A",
        event = StateEvent(
            content = MemberEventContent(displayName = "A", membership = JOIN),
            id = EventId("event1"),
            sender = aliceId,
            roomId = roomId,
            originTimestamp = 1,
            stateKey = aliceId.full
        )
    )
    val bobUser = RoomUser(
        roomId = roomId,
        userId = bobId,
        name = "A",
        event = StateEvent(
            content = MemberEventContent(displayName = "A", membership = JOIN),
            id = EventId("event2"),
            sender = bobId,
            roomId = roomId,
            originTimestamp = 1,
            stateKey = bobId.full
        )
    )

    context("getAll") {
        should("get all users of a room") {
            val scope = CoroutineScope(Dispatchers.Default)

            roomUserRepository.save(
                roomId, mapOf(
                    aliceId to aliceUser,
                    bobId to bobUser
                )
            )

            cut.getAll(roomId, scope).value shouldContainExactly listOf(aliceUser, bobUser)

            scope.cancel()
        }
    }
    context(RoomUserStore::getByOriginalNameAndMembership.name) {
        should("return matching userIds") {
            val user3 = UserId("user3", "server")
            val user4 = UserId("user4", "server")
            roomUserRepository.save(
                roomId, mapOf(
                    aliceId to aliceUser,
                    bobId to bobUser,
                    user3 to RoomUser(
                        roomId = roomId,
                        userId = user3,
                        name = "A",
                        event = StateEvent(
                            content = MemberEventContent(displayName = "A", membership = LEAVE),
                            id = EventId("event3"),
                            sender = user3,
                            roomId = roomId,
                            originTimestamp = 1,
                            stateKey = user3.full
                        )
                    ),
                    user4 to RoomUser(
                        roomId = roomId,
                        userId = user4,
                        name = "AB",
                        event = StateEvent(
                            content = MemberEventContent(displayName = "AB", membership = JOIN),
                            id = EventId("event3"),
                            sender = user4,
                            roomId = roomId,
                            originTimestamp = 1,
                            stateKey = user4.full
                        )
                    )
                )
            )

            cut.getByOriginalNameAndMembership("A", setOf(JOIN), roomId) shouldBe setOf(aliceId, bobId)
        }
    }
})