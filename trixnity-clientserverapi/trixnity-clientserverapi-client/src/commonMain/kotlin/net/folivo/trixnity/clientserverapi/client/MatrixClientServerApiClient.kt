package net.folivo.trixnity.clientserverapi.client

import io.ktor.client.*
import io.ktor.http.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.serialization.json.Json
import net.folivo.trixnity.core.serialization.createEventContentSerializerMappings
import net.folivo.trixnity.core.serialization.createMatrixJson
import net.folivo.trixnity.core.serialization.events.EventContentSerializerMappings

interface IMatrixClientServerApiClient {
    val json: Json
    val eventContentSerializerMappings: EventContentSerializerMappings
    val accessToken: MutableStateFlow<String?>
    val httpClient: MatrixClientServerApiHttpClient
    val authentication: IAuthenticationApiClient
    val server: IServerApiClient
    val users: IUsersApiClient
    val rooms: IRoomsApiClient
    val sync: ISyncApiClient
    val keys: IKeysApiClient
    val media: IMediaApiClient
    val devices: IDevicesApiClient
    val push: IPushApiClient
}

class MatrixClientServerApiClient(
    baseUrl: Url? = null,
    override val json: Json = createMatrixJson(),
    onLogout: suspend (isSoft: Boolean) -> Unit = {},
    override val eventContentSerializerMappings: EventContentSerializerMappings = createEventContentSerializerMappings(),
    httpClientFactory: (HttpClientConfig<*>.() -> Unit) -> HttpClient = { HttpClient(it) },
): IMatrixClientServerApiClient {
    override val accessToken = MutableStateFlow<String?>(null)

    override val httpClient = MatrixClientServerApiHttpClient(
        baseUrl,
        json,
        eventContentSerializerMappings,
        accessToken,
        onLogout,
        httpClientFactory
    )

    override val authentication: IAuthenticationApiClient = AuthenticationApiClient(httpClient)
    override val server: IServerApiClient = ServerApiClient(httpClient)
    override val users: IUsersApiClient = UsersApiClient(httpClient, json, eventContentSerializerMappings)
    override val rooms: IRoomsApiClient = RoomsApiClient(httpClient, eventContentSerializerMappings)
    override val sync: ISyncApiClient = SyncApiClient(httpClient)
    override val keys: IKeysApiClient = KeysApiClient(httpClient, json)
    override val media: IMediaApiClient = MediaApiClient(httpClient)
    override val devices: IDevicesApiClient = DevicesApiClient(httpClient)
    override val push: IPushApiClient = PushApiClient(httpClient)
}
