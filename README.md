![Version](https://maven-badges.herokuapp.com/maven-central/net.folivo/trixnity-core/badge.svg)

# Trixnity - Multiplatform Matrix SDK

Trixnity is a multiplatform [Matrix](matrix.org) SDK written in Kotlin. You can write clients, bots and appservices with
it. This SDK supports JVM (also Android), JS and Native as targets for most modules.
[Ktor](https://github.com/ktorio/ktor) is used for the HTTP client/server and
[kotlinx.serialization](https://github.com/Kotlin/kotlinx.serialization) for the serialization/deserialization.

Trixnity aims to be strongly typed, customizable and easy to use. You can register custom events and Trixnity will take
care, that you can send and receive that type.

**You need help? Ask your questions in [#trixnity:imbitbu.de](https://matrix.to/#/#trixnity:imbitbu.de).**

## Overview

This project contains the following modules, which can be used independently:

- [trixnity-core](/trixnity-core) contains the model and serialization stuff for Matrix.
- [trixnity-olm](/trixnity-olm) implements the wrappers of the
  E2E-olm-library [libolm](https://gitlab.matrix.org/matrix-org/olm) for Kotlin JVM/Android/JS/Native. It also ships the
  olm binaries for Android packages and Windows/Linux binaries on JVM and Native packages.
- [trixnity-api-client](/trixnity-api-client) provides tools for api client modules.
- [trixnity-api-server](/trixnity-api-server) provides tools for api server modules.
- [trixnity-clientserverapi-*](/trixnity-clientserverapi) provides modules to use
  the [Client-Server API](https://spec.matrix.org/latest/client-server-api/).
    - [trixnity-clientserverapi-model](/trixnity-clientserverapi/trixnity-clientserverapi-model) provides shared model
      classes.
    - [trixnity-clientserverapi-client](/trixnity-clientserverapi/trixnity-clientserverapi-client) is the client
      implementation without logic.
    - [trixnity-clientserverapi-server](/trixnity-clientserverapi/trixnity-clientserverapi-server) is the server
      implementation without logic.
- coming soon: [trixnity-serverserverapi-*](/trixnity-serverserverapi) provides modules to use
  the [Server-Server API](https://spec.matrix.org/latest/server-server-api/).
    - [trixnity-serverserverapi-model](/trixnity-serverserverapi/trixnity-serverserverapi-model)  provides shared model
      classes.
    - [trixnity-serverserverapi-client](/trixnity-serverserverapi/trixnity-serverserverapi-client) is the client
      implementation without logic.
    - [trixnity-serverserverapi-server](/trixnity-serverserverapi/trixnity-serverserverapi-server) is the server
      implementation without logic.
- [trixnity-applicationserviceapi-*](/trixnity-applicationserviceapi) provides modules to use
  the [Application Service API](https://spec.matrix.org/latest/application-service-api/).
    - [trixnity-applicationserviceapi-model](/trixnity-applicationserviceapi/trixnity-applicationserviceapi-model)
      provides shared model classes.
    - [trixnity-applicationserviceapi-server](/trixnity-applicationserviceapi/trixnity-applicationserviceapi-server) is
      the server implementation without logic.
- [trixnity-client](/trixnity-client) provides a high level client implementation. It allows you to easily implement
  clients by just rendering data from and passing user interactions to Trixnity. The key features are:
    - [x] exchangeable database
        - [trixnity-client-store-exposed](/trixnity-client/trixnity-client-store-exposed) implements a database for
          trixnity-client with [Exposed](https://github.com/JetBrains/Exposed). This only supports JVM based platforms.
        - [trixnity-client-store-sqldelight](/trixnity-client/trixnity-client-store-sqldelight) implements a database
          for trixnity-client with [sqldelight](https://github.com/cashapp/sqldelight/). This is not actively maintained
          at the moment.
        - We plan to add something like `trixnity-client-store-indexeddb` as a database backend for web in the future.
    - [x] fast cache on top of the database
    - [x] E2E (olm, megolm)
    - [x] verification
    - [x] cross signing
    - [x] room key backup
    - [x] room list
    - [ ] spaces
    - [x] timelines
    - [x] user and room display name calculation
    - [x] asynchronous message sending without caring about E2E stuff or online status
    - [x] media support (thumbnail generation, offline "upload", etc.)
    - [x] redactions
    - [x] notifications

- [trixnity-applicationservice](/trixnity-applicationservice) provides a basic high level application service
  implementation. It does not support advanced features like E2E or persistence at the moment.

If you want to see Trixnity in action, take a look into the [examples](/examples).

### Add Trixnity to you project

Just add the following to your dependencies and fill `<module>` (with e.g. `client`) and `<version>` (with the current
version):

```yml
implementation("net.folivo:trixnity-<module>:<version>")
```

For Trixnity-Client and Trixnity-ClientServerAPI-Client you also need to add a client engine to your project, that you
can find [here](https://ktor.io/docs/http-client-engines.html).

#### Olm-Library

If you are using a module, which depends on `trixnity-olm` you may need to install olm.

- Android: Everything is packaged. There is nothing to do for you.
- JS: You need to provide the url path `/olm.wasm`. The file can be found in the official olm js package.
- JVM: If your platform is not build by Trixnity's CI, you
  can [download or build it yourself](https://gitlab.matrix.org/matrix-org/olm). Make it available to your JVM (e.g.
  with `-Djna.library.path="build/olm"`). You can also look into the `build.gradle.kts` file of `trixnity-olm`
  for an automated way to build olm and e.g. use it for testing.
- Native: If your platform is not supported, feel free to open a merge request for our CI.

## Trixnity-Client

### Create MatrixClient

With `MatrixClient` you have access to the whole library. It can be instantiated by various static functions,
e.g. `MatrixClient.login(...)`. You always need to pass a `StoreFactory` for a Database and a `CouroutineScope`, which
will be used for the lifecycle of the client.

Secrets are also stored in the store. Therefore, you should encrypt the store!

```kotlin
val storeFactory = createStoreFactory()
val scope = CoroutineScope(Dispatchers.Default)

val matrixClient = MatrixClient.fromStore(
    storeFactory = storeFactory,
    scope = scope,
).getOrThrow() ?: MatrixClient.login(
    baseUrl = Url("https://example.org"),
    identifier = User("username"),
    password = "password",
    storeFactory = storeFactory,
    scope = scope,
).getOrThrow()

matrixClient.startSync() // important to fully start the client!
```

### Read data

Most data in Trixnity is wrapped into Kotlins `StateFlow`. This means, that you get the current value, but also every
future values. This is useful when e.g. the display name or the avatar of a user changes, because you only need to
rerender that change and not your complete application.

There are some important data, which are described below:

#### Rooms

To get the room list, call `matrixClient.room.getAll()`. With `matrixClient.room.getById(...)` you can get one room.

#### Users

To get all members of a room, call `matrixClient.user.getAll(...)`. Because room members are loaded lazily, you should
also call `matrixClient.user.loadMembers(...)` as soon as you open a room. With `matrixClient.user.getById(...)` you can
get one user.

#### TimelineEvents

`TimelineEvent`s represent Events in a room timeline. Each `TimelineEvent` points to its previous and
next `TimelineEvent`, so they form a linear graph: `... <-> TimelineEvent <-> TimelineEvent <-> TimelineEvent <-> ...`.
You can use this to navigate threw the graph and e.g. form a list out of it.

A `TimelineEvent` also contains a `Gap` which can be used to determine, if there are missing events, which can be loaded
from the server: `GabBefore <-> TimelineEvent <-> TimelineEvent <-> GapAfter`. If a `TimelineEvent` has a `Gap`, you can
fetch its neighbours by calling `matrixClient.room.fetchMissingEvents(...)`.

You can always get the last known `TimelineEvent` of a room with `matrixClient.room.getLastTimelineEvent(...)`.

The following example will always print the last 20 events of a room:

```kotlin
matrixClient.room.getLastTimelineEvents(roomId, scope)
    .toFlowList(MutableStateFlow(20)) // we always get max. 20 TimelineEvents
    .collectLatest { timelineEvents ->
        timelineEvents.forEach { timelineEvent ->
            val event = timelineEvent.value?.event
            val content = timelineEvent.value?.content?.getOrNull()
            val sender = event?.sender?.let { matrixClient.user.getById(it, roomId, scope).value?.name }
            when {
                content is RoomMessageEventContent -> println("${sender}: ${content.body}")
                content == null -> println("${sender}: not yet decrypted")
                event is MessageEvent -> println("${sender}: $event")
                event is StateEvent -> println("${sender}: $event")
                else -> {
                }
            }
        }
    }
```

#### Outbox

Messages, that were sent with Trixnity can be accessed with `matrixClient.room.getOutbox()` as long as they are not
received (also called "echo") from the matrix server.

### Other operations

Many operations can be done
with [trixnity-clientserverapi-client](/trixnity-clientserverapi/trixnity-clientserverapi-client). You have access to it
via `matrixClient.api`. There are also some high level operations, which are managed by Trixnity. Some of them are
described below.

#### Send messages

With `matrixClient.room.sendMessage(...)` you get access to an extensible DSL to send messages. This messages will be
saved locally and sent as soon as you are online.

```kotlin
// send a text message
matrixClient.room.sendMessage(roomId) {
    text("Hi!")
}
// send an image
matrixClient.room.sendMessage(roomId) {
    image("dino.png", image, ContentType.Image.PNG)
}
```

#### Media

To upload media there is `MediaService` (can be accessed with `matrixClient.media`).

#### Verification

To verify own and other devices there is `VerificationService` (can be accessed with `matrixClient.verification`).

#### Cross Signing

To bootstrap cross signing there is `KeyService` (can be accessed with `matrixClient.key`).

## Trixnity-ClientServerAPI-Client

### Usage

#### Create MatrixClientServerApiClient

Here is a typical example, how to create a `MatrixClientServerApiClient`:

```kotlin
val matrixRestClient = MatrixClientServerApiClient(
    baseUrl = Url("http://host"),
).apply { accessToken.value = "token" }
```

#### Use Matrix Client-Server API

Example 1: You can send messages.

```kotlin
matrixRestClient.room.sendRoomEvent(
    RoomId("awoun3w8fqo3bfq92a", "your.home.server"),
    TextMessageEventContent("hello from platform $Platform")
)
```

Example 2: You can receive different type of events from sync.

```kotlin
matrixRestClient.sync.subscribe<TextMessageEventContent> { println(it.content.body) }
matrixRestClient.sync.subscribe<MemberEventContent> { println("${it.content.displayName} did ${it.content.membership}") }
matrixRestClient.sync.subscribeAllEvents { println(it) }

matrixRestClient.sync.start() // you need to start the sync to receive messages
delay(30.seconds) // wait some time
matrixRestClient.sync.stop() // stop the client
```

## Trixnity-Appservice

The appservice module of Trixnity contains a webserver, which hosts the Matrix Application-Service API.

You also need to add a server engine to your project, that you can find [here](https://ktor.io/docs/engines.html).

### Usage

#### Register module in Ktor

The ktor Application extension `matrixApplicationServiceApiServer` is used to register the appservice endpoints within a
Ktor server. [See here](https://ktor.io/docs/create-server.html) for more information on how to create a Ktor server.

```kotlin
val engine: ApplicationEngine = embeddedServer(CIO, port = 443) {
    matrixAppserviceModule("asToken", handler)
}
engine.start(wait = true)
```

#### Use `DefaultApplicationServiceApiServerHandler` as default handler

The `DefaultApplicationServiceApiServerHandler` implements `ApplicationServiceApiServerHandler`. It makes the
implementation of a Matrix Appservice more abstract and easier. For that it uses `ApplicationServiceEventTxnService`
, `ApplicationServiceUserService` and `ApplicationServiceRoomService`, which you need to implement.

It also allows you to retrieve events with `subscribe` in the same way as
described [here](#use-matrix-client-server-api).

## Logging

This project uses [kotlin-logging](https://github.com/MicroUtils/kotlin-logging). On JVM this needs a logging backend.
You can use for example `implementation("ch.qos.logback:logback-classic:<version>")`.

## Build this project

### Build Olm

Build olm with the `buildOlm` gradle tasks.

### Android SDK

Install the Android SDK with the following packages:

- platforms;android-30
- build-tools
- ndk

Add a file named `local.properties` with the following content in the project root:

```properties
sdk.dir=/path/to/android/sdk
```

### Libraries for c-bindings

Linux:

- cmake `3.22.1` (e.g. by running `sudo ./cmake-3.22.1-linux-x86_64.sh --skip-license --exclude-subdir --prefix=/usr`).
  For 64 bit linux systems: https://cmake.org/files/v3.22/cmake-3.22.0-linux-x86_64.sh
- libncurses5
- ninja-build
- mingw-w64 (Ubuntu: `sudo apt install x86_64-w64-mingw32-g++`)

Windows: Install msys2. Add cmake and run in msys2 mingw64
shell `pacman -S clang mingw-w64-x86_64-cmake mingw-w64-x86_64-ninja mingw-w64-x86_64-toolchain`
. **Important:** Run this command and all gradle tasks within the msys2 **mingw64** shell!
